﻿
using System.Collections.Generic;

namespace BookStore.Domain
{
    public class Categoria
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public ICollection<Livro> Livros { get; set; }//com virtual ele carrega sobre demanda

        public Categoria()
        {
            this.Livros = new List<Livro>(); //com Nelio eu inicializava na prop
        }
    }
}