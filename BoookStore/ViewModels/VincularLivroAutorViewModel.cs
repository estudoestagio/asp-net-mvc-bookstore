﻿using System.ComponentModel.DataAnnotations;

namespace BoookStore.ViewModels
{
    public class VincularLivroAutorViewModel
    {
        //só preciso de AutorId e LivroId para vincular as tabelas
        [Required(ErrorMessage ="*")]
        public int AutorId { get; set; }

        [Required(ErrorMessage = "*")]
        public int LivroId { get; set; }
    }
}