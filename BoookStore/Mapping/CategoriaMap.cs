﻿using BookStore.Domain;
using System.Data.Entity.ModelConfiguration;

namespace BoookStore.Mapping
{
    public class CategoriaMap 
        : EntityTypeConfiguration<Categoria> //segun yo hace referência a la classe de BookStore.Domain
    {
        public CategoriaMap()
        {
            ToTable("Categoria"); //según crea la tabela con este nombre

            HasKey(x => x.Id);
            Property(x => x.Nome).HasMaxLength(30).IsRequired();

            HasMany(x => x.Livros)
                .WithRequired(x => x.Categoria);
        }        
    }
}