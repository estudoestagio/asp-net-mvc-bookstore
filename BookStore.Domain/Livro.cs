﻿using System;
using System.Collections.Generic;

namespace BookStore.Domain
{
    //relacionamento é feito com List ou referenciados os itens com categoriaId e Categoria por exemplo
    public class Livro //objeto é criado por propriedades, métodos e eventos
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string ISBN { get; set; }
        public DateTime DataLancamento { get; set; }

        public int CategoriaId { get; set; } //referencia o id da categoria, o EF vai mapear(pois é um ORM) creo. ajuda a pesquisar mas não é obrigatório
        public Categoria Categoria { get; set; }

        public ICollection<Autor> Autores { get; set; }

        public Livro()
        {
            this.Autores = new List<Autor>();
        }
    }
}