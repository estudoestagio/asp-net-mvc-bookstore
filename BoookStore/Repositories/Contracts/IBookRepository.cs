﻿using BookStore.Domain;
using System;
using System.Collections.Generic;

namespace BoookStore.Repositories.Contracts
{
    public interface IBookRepository : IDisposable
    {
        List<Livro> Get(); // lista os autores
        Livro Get(int id); // obtem autor
        List<Livro> GetByName(string name);
        bool Create(Livro livro); // Salva autor no BD
        bool Update(Livro livro); // atualiza autor no BD
        void Delete(int id); // apaga autor no BD
    }
}