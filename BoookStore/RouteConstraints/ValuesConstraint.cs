﻿using System;
using System.Linq;
using System.Web;
using System.Web.Routing;

namespace BoookStore.RouteConstraints
{
    public class ValuesConstraint : IRouteConstraint
    {
        private readonly string[] validOptions; // vou receber estas informações através do mapeamento deste atributo
        public ValuesConstraint(string options)
        {
            validOptions = options.Split('|');
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        { // método que vÊ se o que eu recebi (validOptions) é válido ou não
            object value;
            if (values.TryGetValue(parameterName, out value) && value != null) 
            {
                return validOptions.Contains(value.ToString(), StringComparer.OrdinalIgnoreCase); // ve se as opções contém algum valor que foi informado no parâmetro
            }
            return false;
        }
    }
}