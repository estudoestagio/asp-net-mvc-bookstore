﻿using BoookStore.Validators;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace BoookStore.ViewModels
{
    public class EditorBookViewModel
    {
        public int Id { get; set; }

        // Classe Livro
        //public int Id { get; set; } //não precisa
        [Required(ErrorMessage = "Nome inválido")]
        [Display(Name = "Nome do Livro")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "ISBN inválido")]
        public string ISBN { get; set; }

        [Required(ErrorMessage = "Data inválida")]
        [Display(Name = "Data de Lançamento")]
        [DataType(DataType.Date)]
        public DateTime DataLancamento { get; set; }

        [Required(ErrorMessage = "Selecione uma categoria")]
        public int CategoriaId { get; set; }
        public SelectList CategoriaOptions { get; set; }

        [CheckAgeValidator]
        public DateTime Age { get; set; }

        //public Categoria Categoria { get; set; } //não precisa

        //public ICollection<Autor> Autores { get; set; } //não precisa
    }
}