﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BoookStore.Validators
{
    public class CheckAgeValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid
            (object value, ValidationContext validationContext)
        {
            DateTime date = (DateTime)value;

            if ((date.Year - 18) > 0) //creo que está mal este raciocinio... lo teste y parece que si está mal
                return new ValidationResult("Somente maiores de 18 anos");

            return ValidationResult.Success;
        }
    }
}