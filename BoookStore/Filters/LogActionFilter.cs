﻿using System;
using System.Diagnostics;
using System.Web.Mvc;
using System.Web.Routing;

namespace BoookStore.Filters
{
    public class LogActionFilter : ActionFilterAttribute //Action Filters e ResultFilters (existem mais 2 dos de action filters: Authorization e Exception)
    {
        // OnActionExecuting: Cuando la action empieza a ser ejecutada                  // OnActionExecuted: Cuando la action ya fue ejecutada    
        // OnResultExecuting: Cuando el resultado de la action empieza a ser ejecutado  // OnActionExecuted: Cuando el resultado de la action acaba de ser ejecutado
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Log("OnActionExecuting", filterContext.RouteData);
        }
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Log("OnActionExecuted", filterContext.RouteData);
        }
        public override void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Log("OnResultExecuting", filterContext.RouteData);
        }
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Log("OnResultExecuted", filterContext.RouteData);
        }

        private void Log(string methodName, RouteData routeData)
        {
            var controllerName = routeData.Values["controller"];
            var actionName = routeData.Values["action"];
            var message = String.Format("{0} controller:{1} action:{2}", methodName, controllerName, actionName);
            Debug.WriteLine(message, "Action Filter Log");
        }
    }
}