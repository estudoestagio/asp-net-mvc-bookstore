﻿using BookStore.Domain;
using System.Data.Entity.ModelConfiguration;

namespace BoookStore.Mapping
{
    public class AutorMap
        : EntityTypeConfiguration<Autor> //segun yo hace referência a la classe de BookStore.Domain
    {
        public AutorMap()
        {
            ToTable("Autor");

            HasKey(x => x.Id);
            Property(x => x.Nome).HasMaxLength(60).IsRequired();

            HasMany(x => x.Livros)
                .WithMany(x => x.Autores)
                .Map(x => x.ToTable("LivroAutor")); //nem precisa fazer no Livro de novo
        }
    }
}