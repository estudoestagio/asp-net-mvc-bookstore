using BookStore.Context;
using BoookStore.Repositories;
using BoookStore.Repositories.Contracts;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace BoookStore
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // AQUI � o DE PARA, cada vez que o da esquerda for chamado, o da direita vai ser instanciado (isso para resolver a inje��o de depend�ncia)             
            //e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<BookStoreDataContext, BookStoreDataContext>();
            container.RegisterType<IAuthorRepository, AuthorRepository>();


            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}