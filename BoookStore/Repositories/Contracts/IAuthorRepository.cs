﻿using BookStore.Domain;
using System;
using System.Collections.Generic;

namespace BoookStore.Repositories.Contracts
{
    public interface IAuthorRepository : IDisposable //IDisposable vai forçar que eu tenha um método dispose onde destroi a conexão com BD(o EF já faz isso, mas posso otimizar com este método)
    {
        List<Autor> Get(); // lista os autores
        Autor Get(int id); // obtem autor
        List<Autor> GetByName(string name); 
        bool Create(Autor autor); // Salva autor no BD
        bool Update(Autor autor); // atualiza autor no BD
        void Delete(int id); // apaga autor no BD
    }
}